declare {
    int i1 = 2 + 2;
    int i2 = 2.0 + 2;
    int i3 = i1 + 2;
    int i4 = i1 + "2";
    int i5 = i1 ^ i3;
    int i6 = 2 + '2' + true + 0x2;
    bool b1 = (i1 == 4) || (i3 <= 2);
    bool b2 = '\x0';
    bool b3 = 2 & b2;
    double x1 = 2.0 + 2;
    double x2 = i1;
    double x3 = .1E+4;
    double x4 = -0.1e-5;
    double x5 = x3 ^ x4;
}

(int r) = function start(){
    int x;
    read(x);
    float y;
    read(y);
    r = 0;
    return;
}