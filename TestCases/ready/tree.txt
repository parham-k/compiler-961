declare {
    Node;
    Tree;
}

type Node {
    int data;
    Node left, right;
}

type Tree {
    Node root;
}

(int r) = function start(){
    Tree t = allocate Tree;
    t.root.data = 2;
    r = 0;
    return;
}