declare
{
int x;
long array[3][4];
char c = 's';
string name = "LULU2";
(int) = calc(int , int);
}

type obj
{
string name;
int value;
}

type stuff
{
obj mystuff;
string color;
}

(int retValue) = function calc(int a, int b)
{
	write(a);
	write(b);
	string z = a + b;  
	%%error
	int ans = a+b;
	retValue = ans;
		
	string mystring = "Compiler Project";
	ans = mystring ;   
	%% error
	obj c1;
	c1.name = 13;
	%%error
	c1.name = "asdf";
	c1.value = 123;
	
	return;
}

(int programValue) = function start()
{
array[0][0] = 123;
if ( c == 234)
	c = 'w';
%%error

lbl:

int myint = 0;
for(int i=0;;i<10;i=i+1)
	myint = myint + i;

if(myint == 0)
	goto lbl;

return;
}
