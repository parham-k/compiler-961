%% A sample program
declare {
int a;
myType;
(int, float) = f1(double b);
}
type myType {
protected int x;
public (float y) = function myFun(int z){
if (z > x) {
y = 23.5; }
else {
y = 15.2; }
return;
}
}
(int r) = function start() {
const double c = 0.23;
double s;
read(s);
float w;
(r, w) = f1(s);
return;
}
(int x, float y) = function f1(int b) {
(y) = a.myFun(b);
x = 0;
return;
}
