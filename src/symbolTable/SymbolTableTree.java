package symbolTable;

import types.Type;

import java.util.LinkedList;
import java.util.Queue;

public class SymbolTableTree {

    private Scope global;
    private Scope currentScope;

    public SymbolTableTree() {
        global = new Scope("GLOBAL");
        currentScope = global;
    }

    public void enterScope(String name) {
        currentScope = currentScope.insertChild(name);
    }

    public void exitScope() {
        currentScope = currentScope.getParent();
    }

    public boolean updateID(String idName, Object value) {
        Scope scope = currentScope;
        while (scope != null)
            if (scope.symbolTable().updateID(idName, value))
                return true;
            else
                scope = scope.getParent();
        return false;
    }

    public boolean insertID(String idName, Type type, Object value) {
        return currentScope.symbolTable().insertID(idName, type, value);
    }

    public boolean insertID(String idName, SymbolTableEntry entry) {
        return currentScope.symbolTable().insertID(idName, entry);
    }

    public boolean hasID(String idName) {
        Scope scope = currentScope;
        while (scope != null)
            if (scope.symbolTable().hasID(idName))
                return true;
            else
                scope = scope.getParent();
        return false;
    }

    public SymbolTableEntry find(String idName) {
        Scope scope = currentScope;
        while (scope != null) {
            SymbolTableEntry currentScopeEntry = scope.symbolTable().find(idName);
            if (currentScopeEntry != null)
                return currentScopeEntry;
            else
                scope = scope.getParent();
        }
        return null;
    }

    public Scope getCurrentScope() {
        return currentScope;
    }

    public Scope getGlobal() {
        return global;
    }

}
