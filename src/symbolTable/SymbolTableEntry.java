package symbolTable;

import enums.AccessModifier;
import types.Type;

public class SymbolTableEntry {

    private Type type;
    private int width;
    private Object value;
    private AccessModifier access;
    private boolean isConst;
    private boolean isRval, isPval; //isReturnValue, isParameterValue ->for functions //Mahdi
    public SymbolTableEntry(Type type, Object value) {
        this.type = type;
        this.width = type.getWidth();
        this.value = value;
        this.access = AccessModifier.PRIVATE;
        this.isConst = false;
        this.isRval = false;
        this.isPval = false;
    }

    public void setConst(boolean isConst) {
        this.isConst = isConst;
    }

    public void setAccess(AccessModifier access) {
        this.access = access;
    }

    public Type getType() {
        return type;
    }

    public int getWidth() {
        return width;
    }

    public Object getValue() {
        return value;
    }

    public void updateValue(Object value) {
        this.value = value;
    }

    public AccessModifier getAccess() {
        return access;
    }

    public String toString() {
        return "type:" + type.getName() + " | width:" + getWidth() + " | value:" + value + " | isConst: " + isConst + " | access: " + access + " | Rval:" + isRval() + " |Pval:" + isPval();
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isConst() {
        return isConst;
    }

    public boolean isRval() {
        return isRval;
    }

    public void setRval(boolean rval) {
        isRval = rval;
    }

    public boolean isPval() {
        return isPval;
    }

    public void setPval(boolean pval) {
        isPval = pval;
    }
}
