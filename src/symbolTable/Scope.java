package symbolTable;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Scope {

    private String name;
    private Scope parent;
    private ArrayList<Scope> children;
    private SymbolTable table;
    private ArrayList<String> errors;

    Scope(String name) {
        children = new ArrayList<>();
        table = new SymbolTable();
        errors = new ArrayList<>();
        this.name = name;
    }

    public Scope insertChild(String name) {
        Scope child = new Scope(name);
        child.parent = this;
        this.children.add(child);
        return child;
    }

    public String getName() {
        return name;
    }

    public Scope getParent() {
        return parent;
    }

    public ArrayList<Scope> getChildren() {
        return children;
    }

    public SymbolTable symbolTable() {
        return table;
    }

    public int getWidth() {
        int w = table.getWidth();
        Queue<Scope> q = new LinkedList<>(children);
        while (q.size() > 0) {
            Scope s = q.remove();
            w += s.symbolTable().getWidth();
            q.addAll(s.getChildren());
        }
        return w;
    }

    public void addError(int line, int pos, String text) {
        String prefix = (line != 0 ? line + ":" + pos : "");
        errors.add(prefix + " " + text);
    }

    public ArrayList<String> getErrors() {
        return errors;
    }

    public boolean hasError() {
        return errors.size() > 0;
    }
}