package symbolTable;

import types.Type;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SymbolTable {

    private Map<String, SymbolTableEntry> tableData;

    public SymbolTable() {
        tableData = new HashMap<>();
    }

    public boolean updateID(String idName, Object value) {
        SymbolTableEntry entry = find(idName);
        if (entry != null) {
            entry.updateValue(value);
            return true;
        } else
            return false;
    }

    public boolean insertID(String idName, Type type, Object value) {
        //System.out.println(idName + " " + type.toString());
        if (!tableData.containsKey(idName.split("#")[0])) {
            SymbolTableEntry entry = new SymbolTableEntry(type, value);
            tableData.put(idName, entry);
            return true;
        } else
            return false;
    }

    public boolean insertID(String idName, SymbolTableEntry entry) {
        //System.out.println(idName + " " + entry.toString());
//        if (!tableData.containsKey(idName)) {
//            tableData.put(idName, entry);
//            return true;
//        } else
//            return false;
        for (String key :
                tableData.keySet()) {
            if (key.split("#")[0].equals(idName))
                return false;
        }
        tableData.put(idName, entry);
        return true;
    }

    public SymbolTableEntry find(String idName) {
        return tableData.getOrDefault(idName, null);
    }

    public int getWidth() {
        int w = 0;
        for (SymbolTableEntry entry : tableData.values())
            w += entry.getWidth();
        return w;
    }

    public String toString() {
        String r_value = "_________________________________________________________________________________\n";

        for (String key :
                tableData.keySet()) {
            r_value += "| name:" + key + " | " + tableData.get(key).toString() + "\n";
        }

        r_value += "_________________________________________________________________________________";
        return r_value;
    }

    public boolean hasID(String idName) {
        return tableData.containsKey(idName);
    }

    public JTable tableGUI() {
        String[] header = {"ID", "TYPE", "WIDTH", "ACCESS", "CONST", "VALUE", "isRval", "isPval"};
        String[][] data = new String[tableData.size()][8];
        int i = 0;
        for (String idName : tableData.keySet()) {
            SymbolTableEntry entry = tableData.get(idName);
            data[i][0] = idName;
            data[i][1] = entry.getType().toString();
            data[i][2] = Integer.toString(entry.getWidth());
            data[i][3] = entry.getAccess().toString();
            data[i][4] = Boolean.toString(entry.isConst());
            data[i][5] = (entry.getValue() != null ? entry.getValue().toString() : "NULL");
            data[i][6] = entry.isRval()?"YES":"NO";
            data[i][7] = entry.isPval()?"YES":"NO";
            ++i;
        }
        return new JTable(data, header);
    }

    public ArrayList<SymbolTableEntry> getRvals()
    {
        ArrayList<SymbolTableEntry> r = new ArrayList<>();
        for (SymbolTableEntry ste :
                tableData.values()) {
            if (ste.isRval())
                r.add(ste);
        }
        return r;
    }

    public Map<String, SymbolTableEntry> getTableData()
    {
        return tableData;
    }
}
