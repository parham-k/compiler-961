package lulu2;// Generated from D:/University/Term 5/Compiler Design/Project/Phase2/Files/src\LULU2.g4 by ANTLR 4.7

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LULU2Parser}.
 */
public interface LULU2Listener extends ParseTreeListener {
    /**
     * Enter a parse tree produced by {@link LULU2Parser#program}.
     *
     * @param ctx the parse tree
     */
    void enterProgram(LULU2Parser.ProgramContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#program}.
     *
     * @param ctx the parse tree
     */
    void exitProgram(LULU2Parser.ProgramContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#ft_dcl}.
     *
     * @param ctx the parse tree
     */
    void enterFt_dcl(LULU2Parser.Ft_dclContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#ft_dcl}.
     *
     * @param ctx the parse tree
     */
    void exitFt_dcl(LULU2Parser.Ft_dclContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#func_dcl}.
     *
     * @param ctx the parse tree
     */
    void enterFunc_dcl(LULU2Parser.Func_dclContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#func_dcl}.
     *
     * @param ctx the parse tree
     */
    void exitFunc_dcl(LULU2Parser.Func_dclContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#args}.
     *
     * @param ctx the parse tree
     */
    void enterArgs(LULU2Parser.ArgsContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#args}.
     *
     * @param ctx the parse tree
     */
    void exitArgs(LULU2Parser.ArgsContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#args_var}.
     *
     * @param ctx the parse tree
     */
    void enterArgs_var(LULU2Parser.Args_varContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#args_var}.
     *
     * @param ctx the parse tree
     */
    void exitArgs_var(LULU2Parser.Args_varContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#type}.
     *
     * @param ctx the parse tree
     */
    void enterType(LULU2Parser.TypeContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#type}.
     *
     * @param ctx the parse tree
     */
    void exitType(LULU2Parser.TypeContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#type_dcl}.
     *
     * @param ctx the parse tree
     */
    void enterType_dcl(LULU2Parser.Type_dclContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#type_dcl}.
     *
     * @param ctx the parse tree
     */
    void exitType_dcl(LULU2Parser.Type_dclContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#var_def}.
     *
     * @param ctx the parse tree
     */
    void enterVar_def(LULU2Parser.Var_defContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#var_def}.
     *
     * @param ctx the parse tree
     */
    void exitVar_def(LULU2Parser.Var_defContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#var_val}.
     *
     * @param ctx the parse tree
     */
    void enterVar_val(LULU2Parser.Var_valContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#var_val}.
     *
     * @param ctx the parse tree
     */
    void exitVar_val(LULU2Parser.Var_valContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#list}.
     *
     * @param ctx the parse tree
     */
    void enterList(LULU2Parser.ListContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#list}.
     *
     * @param ctx the parse tree
     */
    void exitList(LULU2Parser.ListContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#ft_def}.
     *
     * @param ctx the parse tree
     */
    void enterFt_def(LULU2Parser.Ft_defContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#ft_def}.
     *
     * @param ctx the parse tree
     */
    void exitFt_def(LULU2Parser.Ft_defContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#type_def}.
     *
     * @param ctx the parse tree
     */
    void enterType_def(LULU2Parser.Type_defContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#type_def}.
     *
     * @param ctx the parse tree
     */
    void exitType_def(LULU2Parser.Type_defContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#component}.
     *
     * @param ctx the parse tree
     */
    void enterComponent(LULU2Parser.ComponentContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#component}.
     *
     * @param ctx the parse tree
     */
    void exitComponent(LULU2Parser.ComponentContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#access_modifier}.
     *
     * @param ctx the parse tree
     */
    void enterAccess_modifier(LULU2Parser.Access_modifierContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#access_modifier}.
     *
     * @param ctx the parse tree
     */
    void exitAccess_modifier(LULU2Parser.Access_modifierContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#fun_def}.
     *
     * @param ctx the parse tree
     */
    void enterFun_def(LULU2Parser.Fun_defContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#fun_def}.
     *
     * @param ctx the parse tree
     */
    void exitFun_def(LULU2Parser.Fun_defContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#block}.
     *
     * @param ctx the parse tree
     */
    void enterBlock(LULU2Parser.BlockContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#block}.
     *
     * @param ctx the parse tree
     */
    void exitBlock(LULU2Parser.BlockContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#stmt}.
     *
     * @param ctx the parse tree
     */
    void enterStmt(LULU2Parser.StmtContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#stmt}.
     *
     * @param ctx the parse tree
     */
    void exitStmt(LULU2Parser.StmtContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#assign}.
     *
     * @param ctx the parse tree
     */
    void enterAssign(LULU2Parser.AssignContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#assign}.
     *
     * @param ctx the parse tree
     */
    void exitAssign(LULU2Parser.AssignContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#assign_f}.
     *
     * @param ctx the parse tree
     */
    void enterAssign_f(LULU2Parser.Assign_fContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#assign_f}.
     *
     * @param ctx the parse tree
     */
    void exitAssign_f(LULU2Parser.Assign_fContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#var}.
     *
     * @param ctx the parse tree
     */
    void enterVar(LULU2Parser.VarContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#var}.
     *
     * @param ctx the parse tree
     */
    void exitVar(LULU2Parser.VarContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#ref}.
     *
     * @param ctx the parse tree
     */
    void enterRef(LULU2Parser.RefContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#ref}.
     *
     * @param ctx the parse tree
     */
    void exitRef(LULU2Parser.RefContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#expr}.
     *
     * @param ctx the parse tree
     */
    void enterExpr(LULU2Parser.ExprContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#expr}.
     *
     * @param ctx the parse tree
     */
    void exitExpr(LULU2Parser.ExprContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#expr_lvl1}.
     *
     * @param ctx the parse tree
     */
    void enterExpr_lvl1(LULU2Parser.Expr_lvl1Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#expr_lvl1}.
     *
     * @param ctx the parse tree
     */
    void exitExpr_lvl1(LULU2Parser.Expr_lvl1Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#expr_lvl2}.
     *
     * @param ctx the parse tree
     */
    void enterExpr_lvl2(LULU2Parser.Expr_lvl2Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#expr_lvl2}.
     *
     * @param ctx the parse tree
     */
    void exitExpr_lvl2(LULU2Parser.Expr_lvl2Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#expr_lvl3}.
     *
     * @param ctx the parse tree
     */
    void enterExpr_lvl3(LULU2Parser.Expr_lvl3Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#expr_lvl3}.
     *
     * @param ctx the parse tree
     */
    void exitExpr_lvl3(LULU2Parser.Expr_lvl3Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#expr_lvl4}.
     *
     * @param ctx the parse tree
     */
    void enterExpr_lvl4(LULU2Parser.Expr_lvl4Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#expr_lvl4}.
     *
     * @param ctx the parse tree
     */
    void exitExpr_lvl4(LULU2Parser.Expr_lvl4Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#expr_lvl5}.
     *
     * @param ctx the parse tree
     */
    void enterExpr_lvl5(LULU2Parser.Expr_lvl5Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#expr_lvl5}.
     *
     * @param ctx the parse tree
     */
    void exitExpr_lvl5(LULU2Parser.Expr_lvl5Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#expr_lvl6}.
     *
     * @param ctx the parse tree
     */
    void enterExpr_lvl6(LULU2Parser.Expr_lvl6Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#expr_lvl6}.
     *
     * @param ctx the parse tree
     */
    void exitExpr_lvl6(LULU2Parser.Expr_lvl6Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#expr_lvl7}.
     *
     * @param ctx the parse tree
     */
    void enterExpr_lvl7(LULU2Parser.Expr_lvl7Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#expr_lvl7}.
     *
     * @param ctx the parse tree
     */
    void exitExpr_lvl7(LULU2Parser.Expr_lvl7Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#expr_lvl8}.
     *
     * @param ctx the parse tree
     */
    void enterExpr_lvl8(LULU2Parser.Expr_lvl8Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#expr_lvl8}.
     *
     * @param ctx the parse tree
     */
    void exitExpr_lvl8(LULU2Parser.Expr_lvl8Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#expr_lvl9}.
     *
     * @param ctx the parse tree
     */
    void enterExpr_lvl9(LULU2Parser.Expr_lvl9Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#expr_lvl9}.
     *
     * @param ctx the parse tree
     */
    void exitExpr_lvl9(LULU2Parser.Expr_lvl9Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#expr_lvl10}.
     *
     * @param ctx the parse tree
     */
    void enterExpr_lvl10(LULU2Parser.Expr_lvl10Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#expr_lvl10}.
     *
     * @param ctx the parse tree
     */
    void exitExpr_lvl10(LULU2Parser.Expr_lvl10Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#func_call}.
     *
     * @param ctx the parse tree
     */
    void enterFunc_call(LULU2Parser.Func_callContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#func_call}.
     *
     * @param ctx the parse tree
     */
    void exitFunc_call(LULU2Parser.Func_callContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#params}.
     *
     * @param ctx the parse tree
     */
    void enterParams(LULU2Parser.ParamsContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#params}.
     *
     * @param ctx the parse tree
     */
    void exitParams(LULU2Parser.ParamsContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#params_f}.
     *
     * @param ctx the parse tree
     */
    void enterParams_f(LULU2Parser.Params_fContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#params_f}.
     *
     * @param ctx the parse tree
     */
    void exitParams_f(LULU2Parser.Params_fContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#cond_stmt}.
     *
     * @param ctx the parse tree
     */
    void enterCond_stmt(LULU2Parser.Cond_stmtContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#cond_stmt}.
     *
     * @param ctx the parse tree
     */
    void exitCond_stmt(LULU2Parser.Cond_stmtContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#loop_stmt}.
     *
     * @param ctx the parse tree
     */
    void enterLoop_stmt(LULU2Parser.Loop_stmtContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#loop_stmt}.
     *
     * @param ctx the parse tree
     */
    void exitLoop_stmt(LULU2Parser.Loop_stmtContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#jump}.
     *
     * @param ctx the parse tree
     */
    void enterJump(LULU2Parser.JumpContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#jump}.
     *
     * @param ctx the parse tree
     */
    void exitJump(LULU2Parser.JumpContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#label}.
     *
     * @param ctx the parse tree
     */
    void enterLabel(LULU2Parser.LabelContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#label}.
     *
     * @param ctx the parse tree
     */
    void exitLabel(LULU2Parser.LabelContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#const_val}.
     *
     * @param ctx the parse tree
     */
    void enterConst_val(LULU2Parser.Const_valContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#const_val}.
     *
     * @param ctx the parse tree
     */
    void exitConst_val(LULU2Parser.Const_valContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#unary_op}.
     *
     * @param ctx the parse tree
     */
    void enterUnary_op(LULU2Parser.Unary_opContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#unary_op}.
     *
     * @param ctx the parse tree
     */
    void exitUnary_op(LULU2Parser.Unary_opContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#relational1}.
     *
     * @param ctx the parse tree
     */
    void enterRelational1(LULU2Parser.Relational1Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#relational1}.
     *
     * @param ctx the parse tree
     */
    void exitRelational1(LULU2Parser.Relational1Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#relational2}.
     *
     * @param ctx the parse tree
     */
    void enterRelational2(LULU2Parser.Relational2Context ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#relational2}.
     *
     * @param ctx the parse tree
     */
    void exitRelational2(LULU2Parser.Relational2Context ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#muldivmud}.
     *
     * @param ctx the parse tree
     */
    void enterMuldivmud(LULU2Parser.MuldivmudContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#muldivmud}.
     *
     * @param ctx the parse tree
     */
    void exitMuldivmud(LULU2Parser.MuldivmudContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#bool_const}.
     *
     * @param ctx the parse tree
     */
    void enterBool_const(LULU2Parser.Bool_constContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#bool_const}.
     *
     * @param ctx the parse tree
     */
    void exitBool_const(LULU2Parser.Bool_constContext ctx);

    /**
     * Enter a parse tree produced by {@link LULU2Parser#keyword}.
     *
     * @param ctx the parse tree
     */
    void enterKeyword(LULU2Parser.KeywordContext ctx);

    /**
     * Exit a parse tree produced by {@link LULU2Parser#keyword}.
     *
     * @param ctx the parse tree
     */
    void exitKeyword(LULU2Parser.KeywordContext ctx);
}