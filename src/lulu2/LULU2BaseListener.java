package lulu2;

import enums.AccessModifier;
import enums.types;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import symbolTable.Scope;
import symbolTable.SymbolTable;
import symbolTable.SymbolTableEntry;
import symbolTable.SymbolTableTree;
import types.*;
import types.typeChecking.TypeTree;
import utils.ArgsParser;
import utils.TypeRecognizer;
import utils.var_def_function;


import java.util.ArrayList;
import java.util.Map;

public class LULU2BaseListener implements LULU2Listener {

    private SymbolTableTree symbolTableTree;
    private LULU2Parser parser;
    private boolean hasStartFunction;

    public LULU2BaseListener(LULU2Parser parser) {
        symbolTableTree = new SymbolTableTree();
        this.parser = parser;
    }

    @Override
    public void enterProgram(LULU2Parser.ProgramContext ctx) {

    }

    @Override
    public void exitProgram(LULU2Parser.ProgramContext ctx) {
        if (!hasStartFunction)
            symbolTableTree.getCurrentScope().addError(0, 0, "Start function not defined");
    }

    @Override
    public void enterFt_dcl(LULU2Parser.Ft_dclContext ctx) {

    }


    @Override
    public void exitFt_dcl(LULU2Parser.Ft_dclContext ctx) {

    }

    @Override //Mahdi
    public void enterFunc_dcl(LULU2Parser.Func_dclContext ctx) {
        String functionName = ctx.Identifier().toString();
        int cnt = 1;
        Map<String, SymbolTableEntry> ST = symbolTableTree.getCurrentScope().symbolTable().getTableData();

        for (String s :
                ST.keySet()) {
            if (ST.get(s).getType().toString().equals("function") && s.split("#")[0].equals(functionName)) {
                cnt++;
            }
        }
        functionName += "#" + cnt;

        LULU2Parser.ArgsContext returnArgsVar = ctx.args(0);
        ArrayList<LULU2Parser.ArgsContext> returnArgs = new ArrayList<>();
        while (returnArgsVar != null) {
            returnArgs.add(returnArgsVar);
            returnArgsVar = returnArgsVar.args();
        }
        ArrayList<SymbolTableEntry> returns = ArgsParser.parseArgs(parser, returnArgs);

        ArrayList<SymbolTableEntry> params = null;

        if (ctx.args(1) != null) {
            LULU2Parser.ArgsContext paramArgsvar = ctx.args(1);
            ArrayList<LULU2Parser.ArgsContext> paramArgs = new ArrayList<>();
            while (paramArgsvar != null) {
                paramArgs.add(paramArgsvar);
                paramArgsvar = paramArgsvar.args();
            }
            params = ArgsParser.parseArgs(parser, paramArgs);
        } else if (ctx.args_var() != null) {
            LULU2Parser.Args_varContext paramArgsvar = ctx.args_var();
            ArrayList<LULU2Parser.Args_varContext> paramArgs = new ArrayList<>();
            while (paramArgsvar != null) {
                paramArgs.add(paramArgsvar);
                paramArgsvar = paramArgsvar.args_var();
            }
            Map<String, SymbolTableEntry> m = ArgsParser.parseArgsVar(parser, paramArgs);
            params = new ArrayList<>();
            for (String key :
                    m.keySet()) {
                params.add(m.get(key));
            }
        }

        for (SymbolTableEntry ste :
                returns) {
            ste.setRval(true);
        }
        if (params != null)
            for (SymbolTableEntry ste :
                    params) {
                ste.setPval(true);
            }

        FunctionType ft;
        if (params == null)
            ft = new FunctionType(functionName, null, returns.toArray(new SymbolTableEntry[returns.size()]), null);
        else
            ft = new FunctionType(functionName, params.toArray(new SymbolTableEntry[params.size()]), returns.toArray(new SymbolTableEntry[returns.size()]), null);

        if(!symbolTableTree.getCurrentScope().symbolTable().insertID(functionName,ft, null))
            symbolTableTree.getCurrentScope().addError(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(), "There is a variable with this function name!");
    }

    @Override
    public void exitFunc_dcl(LULU2Parser.Func_dclContext ctx) {

    }

    @Override
    public void enterArgs(LULU2Parser.ArgsContext ctx) {

    }

    @Override
    public void exitArgs(LULU2Parser.ArgsContext ctx) {

    }

    @Override
    public void enterArgs_var(LULU2Parser.Args_varContext ctx) {

    }

    @Override
    public void exitArgs_var(LULU2Parser.Args_varContext ctx) {

    }


    @Override
    public void enterType(LULU2Parser.TypeContext ctx) {

    }

    @Override
    public void exitType(LULU2Parser.TypeContext ctx) {

    }

    @Override //Mahdi
    public void enterType_dcl(LULU2Parser.Type_dclContext ctx) {
        String id = ctx.Identifier().toString();
        symbolTableTree.getCurrentScope().symbolTable().insertID(id, new UserType(id), null);
    }

    @Override
    public void exitType_dcl(LULU2Parser.Type_dclContext ctx) {

    }

    @Override
    public void enterVar_def(LULU2Parser.Var_defContext ctx) {

    }

    @Override
    //Alireza
    public void exitVar_def(LULU2Parser.Var_defContext ctx) {
        TokenStream ts = parser.getTokenStream();
        String typeText = ts.getText(ctx.type());
        ArrayList<Type> varTypeList = new ArrayList<>();
        ArrayList<Object> varValues = new ArrayList<>();
        switch (typeText) {
            case "bool":
                varTypeList = var_def_function.var_def_("bool", varValues, ctx, symbolTableTree);
                break;
            case "char":
                varTypeList = var_def_function.var_def_("char", varValues, ctx, symbolTableTree);
                break;
            case "int":
                varTypeList = var_def_function.var_def_("int", varValues, ctx, symbolTableTree);
                break;
            case "long":
                varTypeList = var_def_function.var_def_("long", varValues, ctx, symbolTableTree);
                break;
            case "float":
                varTypeList = var_def_function.var_def_("float", varValues, ctx, symbolTableTree);
                break;
            case "double":
                varTypeList = var_def_function.var_def_("double", varValues, ctx, symbolTableTree);
                break;
            case "string":
                varTypeList = var_def_function.var_def_("string", varValues, ctx, symbolTableTree);
                break;
            default:
                varTypeList = var_def_function.var_def_(typeText, varValues, ctx, symbolTableTree);
                break;

        }
        for (int i = 0; i < ctx.var_val().size(); i++) {
            String idName = ctx.var_val(i).Identifier(0).getText();
            SymbolTableEntry ste = new SymbolTableEntry(varTypeList.get(i), varValues.get(i));
            if (ctx.getText().contains("const"))
                ste.setConst(true);
            if (!symbolTableTree.insertID(idName, ste))
                symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "There is another Variable or function with same Name \' " + idName + " \' .");
        }

    }

    @Override
    public void enterVar_val(LULU2Parser.Var_valContext ctx) {

    }

    @Override
    public void exitVar_val(LULU2Parser.Var_valContext ctx) {

    }

    @Override
    public void enterList(LULU2Parser.ListContext ctx) {

    }

    @Override
    public void exitList(LULU2Parser.ListContext ctx) {

    }

    @Override
    public void enterFt_def(LULU2Parser.Ft_defContext ctx) {

    }

    @Override
    public void exitFt_def(LULU2Parser.Ft_defContext ctx) {

    }

    // Parham
    @Override
    public void enterType_def(LULU2Parser.Type_defContext ctx) {
        boolean error = false;
        String typeName = ctx.Identifier(0).getText();
        symbolTableTree.enterScope("type_def: " + typeName);
        if (!symbolTableTree.getGlobal().symbolTable().hasID(typeName)) {
            symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "Type " + typeName + " not declared");
            error = true;
        }
        Scope typeScope = symbolTableTree.getCurrentScope();
        UserType userType = new UserType(typeName, typeScope);
        if (ctx.Identifier(1) != null) {
            String superClassName = ctx.Identifier(1).getText();
            SymbolTableEntry superClassEntry = symbolTableTree.getGlobal().symbolTable().find(superClassName);
            if (superClassEntry != null)
                userType = new UserType(typeName, typeScope, superClassEntry.getType());
            else {
                error = true;
                typeScope.addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "SuperType " + superClassName + " not declared");
            }
        }
        userType.setScope(typeScope);
        if (!error) {
            SymbolTableEntry userTypeEntry = symbolTableTree.getGlobal().symbolTable().find(typeName);
            userTypeEntry.setType(userType);
        }
    }

    // Parham
    @Override
    public void exitType_def(LULU2Parser.Type_defContext ctx) {
        TokenStream ts = parser.getTokenStream();
        for (LULU2Parser.ComponentContext context : ctx.component()) {
            AccessModifier access = AccessModifier.PRIVATE;
            if (context.access_modifier() != null)
                switch (ts.getText(context.access_modifier())) {
                    case "private":
                        access = AccessModifier.PRIVATE;
                        break;
                    case "public":
                        access = AccessModifier.PUBLIC;
                        break;
                    case "protected":
                        access = AccessModifier.PROTECTED;
                        break;
                }
            if (context.var_def() != null)
                for (int i = 0; i < context.var_def().var_val().size(); i++) {
                    String idName = context.var_def().var_val(i).Identifier(0).getText();
                    SymbolTableEntry entry = symbolTableTree.find(idName);
                    if (entry != null)
                        entry.setAccess(access);
                    else
                        symbolTableTree.getCurrentScope().addError(context.start.getLine(), context.start.getCharPositionInLine(),
                                "ID " + idName + " not defined");
                }
            else if (context.fun_def() != null) {
                int idIndex = 0;
                while (!context.fun_def().getChild(idIndex).getText().equals("function")) ++idIndex;
                ++idIndex;
                String functionName = context.fun_def().getChild(idIndex).getText();
                SymbolTableEntry entry = symbolTableTree.find(functionName);
                if (entry != null)
                    entry.setAccess(access);
            }
        }
        symbolTableTree.exitScope();
    }

    @Override
    public void enterComponent(LULU2Parser.ComponentContext ctx) {

    }

    @Override
    public void exitComponent(LULU2Parser.ComponentContext ctx) {

    }

    @Override
    public void enterAccess_modifier(LULU2Parser.Access_modifierContext ctx) {

    }

    @Override
    public void exitAccess_modifier(LULU2Parser.Access_modifierContext ctx) {

    }

    // Parham
    @Override
    public void enterFun_def(LULU2Parser.Fun_defContext ctx) {
        String func_protoType = "";
        String functionName = "";
        for (int i = 0; i < ctx.getChildCount(); i++)
            if (ctx.getChild(i).getText().equals("function"))
                functionName = ctx.getChild(i + 1).getText();

        symbolTableTree.enterScope(functionName);

        LULU2Parser.Args_varContext returnArgsVar = ctx.args_var(0);
        LULU2Parser.Args_varContext paramArgsVar = ctx.args_var(1);
        ArrayList<LULU2Parser.Args_varContext> returnArgs = new ArrayList<>();
        ArrayList<LULU2Parser.Args_varContext> paramArgs = new ArrayList<>();

        while (returnArgsVar != null) {
            returnArgs.add(returnArgsVar);
            returnArgsVar = returnArgsVar.args_var();
        }

        while (paramArgsVar != null) {
            paramArgs.add(paramArgsVar);
            paramArgsVar = paramArgsVar.args_var();
        }

        boolean isStartFunction = functionName.equals("start");

        Map<String, SymbolTableEntry> returnSymbols = ArgsParser.parseArgsVar(parser, returnArgs);
        isStartFunction &= (returnSymbols.size() == 1);
        func_protoType += "(";
        for (String idName : returnSymbols.keySet()) {
            SymbolTableEntry entry = returnSymbols.get(idName);
            func_protoType += entry.getType() + ",";
            entry.setRval(true);
            symbolTableTree.insertID(idName, entry);
            isStartFunction &= entry.getType().isEqual(new IntegerType());
        }
        func_protoType = func_protoType.substring(0, func_protoType.length() - 1);
        func_protoType += ")" + functionName + "(";


        Map<String, SymbolTableEntry> paramSymbols = ArgsParser.parseArgsVar(parser, paramArgs);
        for (String idName : paramSymbols.keySet()) {
            SymbolTableEntry entry = paramSymbols.get(idName);
            func_protoType += entry.getType() + ",";
            entry.setPval(true);
            symbolTableTree.insertID(idName, entry);
        }
        if (paramSymbols.keySet().size() != 0)
            func_protoType = func_protoType.substring(0, func_protoType.length() - 1);
        func_protoType += ")";

        isStartFunction &= (paramSymbols.size() == 0);

        if (isStartFunction && hasStartFunction)
            symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "Start function is already defined");
        else if (isStartFunction)
            hasStartFunction = true;

        boolean hasDeclared = false;
        for (SymbolTableEntry ste :
                symbolTableTree.getGlobal().symbolTable().getTableData().values()) {
            if (ste.getType().toString().equals("function"))
                if (func_protoType.equals(((FunctionType) (ste.getType())).MyToString())) {
                    hasDeclared = true;
                    break;
                }
        }
        if (!hasDeclared && !functionName.equals("start") && symbolTableTree.getCurrentScope() == symbolTableTree.getGlobal())
            symbolTableTree.getCurrentScope().addError(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(),
                    "This function has not been declared!");

    }

    // Parham
    @Override
    public void exitFun_def(LULU2Parser.Fun_defContext ctx) {

        boolean status = true; //for checking if return paramater has not been set

        for (SymbolTableEntry ste : symbolTableTree.getCurrentScope().symbolTable().getRvals()) {
            if (ste.getValue() == null)
                status = false;
        }
        if (status == false)
            symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "A return value has not been initialized");
        symbolTableTree.exitScope();
    }

    @Override
    public void enterBlock(LULU2Parser.BlockContext ctx) {
        symbolTableTree.enterScope("BLOCK");
    }

    @Override
    public void exitBlock(LULU2Parser.BlockContext ctx) {
        symbolTableTree.exitScope();
    }

    @Override
    public void enterStmt(LULU2Parser.StmtContext ctx) {
        if (ctx.getText().equals("break;") || ctx.getText().equals("continue;")) {
            if (loopDepth <= 0)
                symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "ERROR! \"Using break or continue out of loop!\"");
        } else if (ctx.getChild(0).getText().equals("destruct")) {
            int dimensionCount = 0, j = 1;
            while (j < ctx.getChildCount() && ctx.getChild(j).getText().equals("[")) {
                j += 2;
                ++dimensionCount;
            }
            SymbolTableEntry entry = symbolTableTree.find(ctx.Identifier().getText());
            if (entry == null)
                symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), " ID " + ctx.Identifier().getText() + " not defined");
            else if (!(entry.getType() instanceof ArrayType) && dimensionCount > 0)
                symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), " ID " + ctx.Identifier().getText() + " not an array");
            else if (((ArrayType) entry.getType()).getDimensions().length != dimensionCount)
                symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), ((ArrayType) entry.getType()).getDimensions().length + " []s expected");
        }
    }

    @Override
    public void exitStmt(LULU2Parser.StmtContext ctx) {
        if ((ctx.getText().contains("=")) && ctx.assign() != null) {
            String var = ctx.assign().var(0).ref(0).Identifier().getText();
            if (ctx.assign() == null || ctx.assign().assign_f() == null)
                return;
            String val = ctx.assign().assign_f().getText();
            TypeTree tt = null;

            SymbolTableEntry ste = symbolTableTree.find(var);
            if (ste == null) {
                symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "Variable " + var + " doesn't exist.");
                return;
            }
            if (ste.isConst()) {
                symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "Variable " + var + " is Constant.you can't change its value.");
                return;
            }
            types valType = TypeRecognizer.TypeRecognize(val);
            switch (ste.getType().getName()) {

                case "bool":
                    tt = new TypeTree(ctx.assign().assign_f().expr(), symbolTableTree);
                    if (BoolType.isBool(tt.getRootType()))//valType == types.t_bool || valType==types.t_long || valType==types.t_int || valType == types.t_char
                        symbolTableTree.updateID(var, val);
                    else {
                        symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter boolean to variable ");
                    }
                    break;

                case "char":
                    tt = new TypeTree(ctx.assign().assign_f().expr(), symbolTableTree);
                    if (CharType.isChar(tt.getRootType())) {
                        symbolTableTree.updateID(var, val);
                    } else {
                        symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter a char in single quotation.");
                    }
                    break;

                case "int":
                    tt = new TypeTree(ctx.assign().assign_f().expr(), symbolTableTree);
                    if (IntegerType.isInteger(tt.getRootType()))
                        symbolTableTree.updateID(var, 1);
                    else {
                        symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter integer to variable .");
                    }
                    break;

                case "long":
                    tt = new TypeTree(ctx.assign().assign_f().expr(), symbolTableTree);
                    if (LongType.isLong(tt.getRootType()))
                        symbolTableTree.updateID(var, Long.parseLong(val));
                    else {
                        symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter Long to variable ");

                    }
                    break;

                case "float":
                    tt = new TypeTree(ctx.assign().assign_f().expr(), symbolTableTree);
                    if (tt.getRootType() instanceof FloatType)
                        symbolTableTree.updateID(var, Float.parseFloat(val));
                    else {
                        symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter Float to variable ");
                    }
                    break;

                case "double":
                    tt = new TypeTree(ctx.assign().assign_f().expr(), symbolTableTree);
                    if (DoubleType.isDouble(tt.getRootType()))
                        symbolTableTree.updateID(var, Double.parseDouble(val));
                    else {
                        symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter Double to variable ");
                    }
                    break;

                case "string":
                    tt = new TypeTree(ctx.assign().assign_f().expr(), symbolTableTree);
                    if (tt.getRootType() instanceof StringType)
                        symbolTableTree.updateID(var, val.substring(1, val.length() - 1));
                    else {
                        symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "you should enter String in var ");
                    }
                    break;

            }
        }
    }

    @Override
    public void enterAssign(LULU2Parser.AssignContext ctx) {
        String id = null;
        if (ctx.func_call() != null && ctx.func_call().getText().startsWith("sizeof"))
            id = ctx.var(0).getText();
        if (id != null) {
            SymbolTableEntry ste = symbolTableTree.find(id);
            if (ste == null)
                symbolTableTree.getCurrentScope().addError(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(), id + " has not been declared.");
            else if (!ste.getType().toString().equals("long"))
                symbolTableTree.getCurrentScope().addError(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(), "sizeof() return value must be long");
        }
    }

    @Override
    public void exitAssign(LULU2Parser.AssignContext ctx) {

    }

    @Override
    public void enterAssign_f(LULU2Parser.Assign_fContext ctx) {

    }

    @Override
    public void exitAssign_f(LULU2Parser.Assign_fContext ctx) {

    }

    @Override
    public void enterVar(LULU2Parser.VarContext ctx) {

    }

    @Override
    public void exitVar(LULU2Parser.VarContext ctx) {

    }

    @Override
    public void enterRef(LULU2Parser.RefContext ctx) {
        SymbolTableEntry entry = symbolTableTree.find(ctx.Identifier().getText());
        if (entry != null) {
            int dimensions = 0, i = 1;
            while (i < ctx.getChildCount() && ctx.getChild(i).getText().equals("[")) {
                i += 3;
                ++dimensions;
            }
            int expected = (entry.getType() instanceof ArrayType ? ((ArrayType) entry.getType()).getDimensions().length : 0);
            if (expected == 0 && dimensions > 0)
                symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(),
                        "Can't index non-array type " + entry.getType());
            else if (expected != dimensions)
                symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(),
                        "Wrong indexing, expected  " + expected + " got " + dimensions);
        }
    }

    @Override
    public void exitRef(LULU2Parser.RefContext ctx) {

    }

    @Override
    public void enterExpr(LULU2Parser.ExprContext ctx) {

    }

    @Override
    public void exitExpr(LULU2Parser.ExprContext ctx) {
    }

    @Override
    public void enterExpr_lvl1(LULU2Parser.Expr_lvl1Context ctx) {

    }

    @Override
    public void exitExpr_lvl1(LULU2Parser.Expr_lvl1Context ctx) {

    }

    @Override
    public void enterExpr_lvl2(LULU2Parser.Expr_lvl2Context ctx) {

    }

    @Override
    public void exitExpr_lvl2(LULU2Parser.Expr_lvl2Context ctx) {

    }

    @Override
    public void enterExpr_lvl3(LULU2Parser.Expr_lvl3Context ctx) {

    }

    @Override
    public void exitExpr_lvl3(LULU2Parser.Expr_lvl3Context ctx) {

    }

    @Override
    public void enterExpr_lvl4(LULU2Parser.Expr_lvl4Context ctx) {

    }

    @Override
    public void exitExpr_lvl4(LULU2Parser.Expr_lvl4Context ctx) {

    }

    @Override
    public void enterExpr_lvl5(LULU2Parser.Expr_lvl5Context ctx) {

    }

    @Override
    public void exitExpr_lvl5(LULU2Parser.Expr_lvl5Context ctx) {

    }

    @Override
    public void enterExpr_lvl6(LULU2Parser.Expr_lvl6Context ctx) {

    }

    @Override
    public void exitExpr_lvl6(LULU2Parser.Expr_lvl6Context ctx) {

    }

    @Override
    public void enterExpr_lvl7(LULU2Parser.Expr_lvl7Context ctx) {

    }

    @Override
    public void exitExpr_lvl7(LULU2Parser.Expr_lvl7Context ctx) {

    }

    @Override
    public void enterExpr_lvl8(LULU2Parser.Expr_lvl8Context ctx) {

    }

    @Override
    public void exitExpr_lvl8(LULU2Parser.Expr_lvl8Context ctx) {

    }

    @Override
    public void enterExpr_lvl9(LULU2Parser.Expr_lvl9Context ctx) {

    }

    @Override
    public void exitExpr_lvl9(LULU2Parser.Expr_lvl9Context ctx) {

    }

    @Override
    public void enterExpr_lvl10(LULU2Parser.Expr_lvl10Context ctx) {

    }

    @Override
    public void exitExpr_lvl10(LULU2Parser.Expr_lvl10Context ctx) {
    }

    @Override
    public void enterFunc_call(LULU2Parser.Func_callContext ctx) {
        String var_name = null;
        if (ctx.getText().startsWith("read") || ctx.getText().startsWith("write"))
            if (ctx.var() != null)
                var_name = ctx.var().getText();

        if (var_name != null) {
            SymbolTableEntry ste = symbolTableTree.find(var_name);
            if (ste == null)
                symbolTableTree.getCurrentScope().addError(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(), var_name + " has not been declared");
            else {
                if (!ste.getType().toString().equals("int"))
                    symbolTableTree.getCurrentScope().addError(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine(), "read and write function's return value must be int");
            }
        }
    }

    @Override
    public void exitFunc_call(LULU2Parser.Func_callContext ctx) {

    }

    @Override
    public void enterParams(LULU2Parser.ParamsContext ctx) {

    }

    @Override
    public void exitParams(LULU2Parser.ParamsContext ctx) {

    }

    @Override
    public void enterParams_f(LULU2Parser.Params_fContext ctx) {

    }

    @Override
    public void exitParams_f(LULU2Parser.Params_fContext ctx) {

    }

    @Override
    public void enterCond_stmt(LULU2Parser.Cond_stmtContext ctx) {

    }

    @Override
    public void exitCond_stmt(LULU2Parser.Cond_stmtContext ctx) {
        if (ctx.expr() != null) {
            TypeTree tt = new TypeTree(ctx.expr(), symbolTableTree);
            if (!BoolType.isBool(tt.getRootType()))
                symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(),
                        "Conditional expression must be of bool type");
        }
    }

    private int loopDepth = 0;

    @Override
    public void enterLoop_stmt(LULU2Parser.Loop_stmtContext ctx) {
        loopDepth++;
    }

    @Override
    public void exitLoop_stmt(LULU2Parser.Loop_stmtContext ctx) {
        TypeTree tt = new TypeTree(ctx.expr(), symbolTableTree);
        if (!BoolType.isBool(tt.getRootType()))
            symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(),
                    "Loop expression must be bool type");
        loopDepth--;
    }

    @Override
    public void enterJump(LULU2Parser.JumpContext ctx) {
        String id = ctx.getText().substring(4);
        SymbolTableEntry ste = symbolTableTree.find(id);
        if (ste == null || (ste != null && !ste.getType().toString().equals("label"))) {
            //System.out.println("ERROR! " + id + " Label not found.");
            symbolTableTree.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), id + " Label not found.");
        }
    }

    @Override
    public void exitJump(LULU2Parser.JumpContext ctx) {

    }

    @Override
    public void enterLabel(LULU2Parser.LabelContext ctx) {
        String labelName = ctx.Identifier().getText();
        symbolTableTree.insertID(labelName, new LabelType(), null);
    }

    @Override
    public void exitLabel(LULU2Parser.LabelContext ctx) {

    }

    @Override
    public void enterConst_val(LULU2Parser.Const_valContext ctx) {

    }

    @Override
    public void exitConst_val(LULU2Parser.Const_valContext ctx) {

    }

    @Override
    public void enterUnary_op(LULU2Parser.Unary_opContext ctx) {

    }

    @Override
    public void exitUnary_op(LULU2Parser.Unary_opContext ctx) {

    }

    @Override
    public void enterRelational1(LULU2Parser.Relational1Context ctx) {

    }

    @Override
    public void exitRelational1(LULU2Parser.Relational1Context ctx) {

    }

    @Override
    public void enterRelational2(LULU2Parser.Relational2Context ctx) {

    }

    @Override
    public void exitRelational2(LULU2Parser.Relational2Context ctx) {

    }

    @Override
    public void enterMuldivmud(LULU2Parser.MuldivmudContext ctx) {

    }

    @Override
    public void exitMuldivmud(LULU2Parser.MuldivmudContext ctx) {

    }

    @Override
    public void enterBool_const(LULU2Parser.Bool_constContext ctx) {

    }

    @Override
    public void exitBool_const(LULU2Parser.Bool_constContext ctx) {

    }

    @Override
    public void enterKeyword(LULU2Parser.KeywordContext ctx) {

    }

    @Override
    public void exitKeyword(LULU2Parser.KeywordContext ctx) {

    }

    @Override
    public void visitTerminal(TerminalNode terminalNode) {

    }

    @Override
    public void visitErrorNode(ErrorNode errorNode) {

    }

    @Override
    public void enterEveryRule(ParserRuleContext parserRuleContext) {

    }

    @Override
    public void exitEveryRule(ParserRuleContext parserRuleContext) {

    }

    public SymbolTableTree getSymbolTableTree() {
        return symbolTableTree;
    }
}
