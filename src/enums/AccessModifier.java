package enums;

public enum AccessModifier {

    PRIVATE, PUBLIC, PROTECTED;

}
