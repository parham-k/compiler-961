package types;

public class BoolType extends Type {

    public BoolType() {
        super("bool", 1);
    }

    public static char toChar(boolean value) {
        return value ? '\1' : '\0';
    }

    public static boolean isBool(Type type){
        return (type instanceof IntegerType) || (type instanceof BoolType)
                || (type instanceof LongType) || (type instanceof CharType);
    }

}
