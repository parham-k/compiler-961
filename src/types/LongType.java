package types;

public class LongType extends Type {

    public LongType() {
        super("long", 8);
    }

    public static boolean toBool(long value) {
        return value != 0;
    }

    public static boolean isLong(Type type){
        return (type instanceof IntegerType) || (type instanceof BoolType)
                || (type instanceof LongType) || (type instanceof CharType);
    }

}
