package types;

import symbolTable.Scope;

public class UserType extends Type {

    private UserType superType;
    private Scope scope;

    public UserType(String name, Scope scope) {
        this(name, scope, null);
    }

    public UserType(String name, Scope scope, Type superType) {
        super(name, scope.getWidth());
        this.scope = scope;
        this.superType = (UserType) superType;
    }

    public UserType(String name) {
        super(name, 4);
    }

    public int getWidth() {
        return scope != null ? scope.getWidth() : 4;
    }

    public UserType getSuperType() {
        return superType;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    @Override
    public String toString() {
        return super.toString() + (superType != null ? ": " + superType.toString() : "");
    }
}
