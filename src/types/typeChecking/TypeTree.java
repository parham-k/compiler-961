package types.typeChecking;

import lulu2.LULU2Parser;
import org.antlr.v4.runtime.tree.ParseTree;
import symbolTable.SymbolTableTree;
import types.*;
import utils.TypeParser;

import java.util.LinkedList;
import java.util.Queue;

class Node {
    String value;
    Type type;
}

public class TypeTree {

    private Type root;
    private SymbolTableTree symbolTableTree;

    public TypeTree(LULU2Parser.ExprContext expr, SymbolTableTree symbolTableTree) {
        this.symbolTableTree = symbolTableTree;
        Queue<Node> postOrderString = new LinkedList<>();
        postOrder(expr.getChild(0), postOrderString);
        root = postOrderString.remove().type;
    }

    private void postOrder(ParseTree node, Queue<Node> result) {
        if (node != null) {
            for (int i = 0; i < node.getChildCount(); i++)
                postOrder(node.getChild(i), result);
            if (node.getChildCount() == 0 && !node.getText().matches("[()]")) {
                Node v = new Node();
                if (!isOperation(node.getText()))
                    v.type = TypeParser.getValueType(node.getText(), symbolTableTree);
                v.value = node.getText();
                result.add(v);
            } else if (node.getChildCount() == 2) {
                String op = result.remove().value;
                Type type = result.remove().type;
                Node v = new Node();
                v.type = unify(op, type);
                result.add(v);
            } else if (node.getChildCount() == 3 && !node.getChild(0).getText().equals("(")) {
                Type t1 = result.remove().type;
                String op = result.remove().value;
                Type t2 = result.remove().type;
                Node v = new Node();
                v.type = unify(t1, op, t2);
                result.add(v);
            }
        }
    }

    public Type getRootType() {
        return root;
    }

    private Type unify(String op, Type type) {
        if (op.equals("!") && BoolType.isBool(type))
            return new BoolType();
        else if (op.equals("~") && IntegerType.isInteger(type))
            return new IntegerType();
        else if (op.equals("-") && IntegerType.isInteger(type))
            return new IntegerType();
        else if (op.equals("-") && DoubleType.isDouble(type))
            return new DoubleType();
        symbolTableTree.getCurrentScope().addError(0, 0, "Can't unify: " + op + " " + type);
        return new IntegerType();
    }

    private Type unify(Type t1, String op, Type t2) {
        if (isRelOp(op)
                && (IntegerType.isInteger(t1) || DoubleType.isDouble(t1))
                && (IntegerType.isInteger(t2) || DoubleType.isDouble(t2))) {
            return new BoolType();
        } else if (isArithmetic(op)
                && DoubleType.isDouble(t1) && DoubleType.isDouble(t2)) {
            return Type.getLUB(t1, t2);
        } else if (isLogical(op)
                && BoolType.isBool(t1) && BoolType.isBool(t2)) {
            return new BoolType();
        } else if (isBitwise(op)
                && IntegerType.isInteger(t1) && IntegerType.isInteger(t2))
            return new IntegerType();
        symbolTableTree.getCurrentScope().addError(0, 0, "Can't unify: " + t1 + " " + op + " " + t2);
        return new IntegerType();
    }

    private boolean isRelOp(String op) {
        return op.matches("==|!=|>=|<=|>|<");
    }

    private boolean isBitwise(String op) {
        return op.matches("[|&^]");
    }

    private boolean isLogical(String op) {
        return op.matches("\\|\\||&&");
    }

    private boolean isArithmetic(String op) {
        return op.matches("[+\\-*/%]");
    }

    private boolean isOperation(String op) {
        return op.matches("==|!=|>=|<=|>|<|[|&^]|\\|\\||&&|[+\\-*/%]");
    }

}
