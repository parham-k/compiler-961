package types;

public class IntegerType extends Type {

    public IntegerType() {
        super("int", 4);
    }

    public static long toLong(int value) {
        return (long) value;
    }

    public static double toDouble(int value) {
        return (double) value;
    }

    public static boolean isInteger(Type type){
        return (type instanceof IntegerType) || (type instanceof BoolType)
                || (type instanceof LongType) || (type instanceof CharType);
    }

}
