package types;

public class Type {

    private String name;
    private int width;

    public Type(String name, int width) {
        this.name = name;
        this.width = width;
    }

    public String getName() {
        return name;
    }

    public int getWidth() {
        return width;
    }

    public boolean isEqual(Type t) {
        return this.name.equals(t.name);
    }

    @Override
    public String toString() {
        return name;
    }

    public static Type getLUB(Type t1, Type t2) {
        if (!(t1 instanceof UserType) && !(t2 instanceof UserType))
            return (t1.getWidth() > t2.getWidth() ? t1 : t2);
        else if (t1 instanceof UserType && t2 instanceof UserType)
            if (((UserType) t1).getSuperType() == t2)
                return t1;
            else if (((UserType) t2).getSuperType() == t1)
                return t2;
        return null;
    }

}
