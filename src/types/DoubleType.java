package types;

public class DoubleType extends Type {

    public DoubleType() {
        super("double", 8);
    }

    public static boolean isDouble(Type type){
        return (type instanceof IntegerType) || (type instanceof BoolType)
                || (type instanceof LongType) || (type instanceof CharType)
                || (type instanceof DoubleType) || (type instanceof FloatType);
    }

}
