package types;

import org.omg.CORBA.Object;
import symbolTable.Scope;
import symbolTable.SymbolTableEntry;

import java.io.ObjectInputStream;

public class FunctionType extends Type {

    private int width;
    private SymbolTableEntry[] params, returns;

        public FunctionType(String name, SymbolTableEntry[] params, SymbolTableEntry[] returns, Scope scope) {
        super(name, 0);
        this.params = params;
        this.returns = returns;
        this.width = (scope == null ? 0 : scope.getWidth());
        if(params!=null)
            for (SymbolTableEntry entry : params) this.width += entry.getWidth();
        for (SymbolTableEntry entry : returns) this.width += entry.getWidth();
    }

    @Override
    public String toString() {
        return "function";
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    public SymbolTableEntry[] getParams() {
        return params;
    }

        public SymbolTableEntry[] getReturns() {
        return returns;
    }

    public String MyToString()
    {
        String tmp="(";
        for (SymbolTableEntry ste :
                returns) {
            tmp += ste.getType().toString()+",";
        }
        tmp = tmp.substring(0,tmp.length()-1);

        tmp+=")" + getName().split("#")[0] + "(";

        if(params!=null)
        {
            for (SymbolTableEntry ste :
                    params) {
                tmp += ste.getType().toString()+",";
            }
            tmp = tmp.substring(0,tmp.length()-1);
        }
        tmp+=")";
        return tmp;
    }
}
