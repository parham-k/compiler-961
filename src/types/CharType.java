package types;

public class CharType extends Type {

    public CharType() {
        super("char", 2);
    }

    public static int toInt(char c) {
        return (int) c;
    }

    public static boolean isChar(Type type) {
        return (type instanceof IntegerType) || (type instanceof BoolType)
                || (type instanceof LongType) || (type instanceof CharType);
    }

}
