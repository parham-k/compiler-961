package types;

public class FloatType extends Type {

    public FloatType() {
        super("float", 4);
    }

    public static double toDouble(float value) {
        return (double) value;
    }

}
