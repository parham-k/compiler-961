package types;

public class StringType extends Type {

    public StringType(int length) {
        super("string", 2 * length);
    }

}
