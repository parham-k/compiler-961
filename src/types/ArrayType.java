package types;

public class ArrayType extends Type {

    private Type baseType;
    private int[] dimensions;

    public ArrayType(Type baseType, int[] dimensions) {
        super("array", 0);
        this.baseType = baseType;
        this.dimensions = dimensions;
        this.baseType = baseType;
    }

    public int getWidth() {
        int p = 1;
        for (int d : dimensions)
            p *= d;
        return p * baseType.getWidth();
    }

    public boolean isEqual(Type t) {
        return t instanceof ArrayType &&
                ((ArrayType) t).baseType.isEqual(baseType) &&
                ((ArrayType) t).dimensions.length == dimensions.length;
    }

    public int[] getDimensions() {
        return dimensions;
    }
}
