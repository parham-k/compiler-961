package utils;

import enums.types;
import lulu2.LULU2Parser;
import org.antlr.v4.runtime.CodePointBuffer;
import org.antlr.v4.runtime.TokenStream;
import symbolTable.SymbolTableTree;
import types.*;
import types.typeChecking.TypeTree;

import java.util.ArrayList;

/**
 * Created by Alireza on 1/24/2018.
 */
//Alireza
public class var_def_function {

    private SymbolTableTree symbolTableTree;
    private LULU2Parser parser;
    TokenStream ts = parser.getTokenStream();

    public static ArrayList<Type> var_def_(String type, ArrayList<Object> varValues, LULU2Parser.Var_defContext ctx, SymbolTableTree stt) {

        ArrayList<Type> varTypeList = new ArrayList<>();
        ArrayList<Integer> Parameters = new ArrayList<>();  // array parameters for n dimention
        //ArrayList<Object> varValues = new ArrayList<>();
        TypeTree tt = null;
        switch (type) {
            case "bool":

                for (int i = 0; i < ctx.var_val().size(); i++)
                    if (ctx.var_val(i).getText().contains("[")) {

                        for (int k = 0; k < ctx.var_val(i).Int_const().size(); k++)
                            Parameters.add(Integer.parseInt(ctx.var_val(i).Int_const(k).toString()));

                        int[] x = new int[Parameters.size()];
                        for (int j = 0; j < Parameters.size(); j++) {
                            x[j] = Parameters.get(j);   //Parameters in ArrayList
                        }
                        varTypeList.add(new ArrayType(new BoolType(), x));
                        varValues.add(null);
                        Parameters.clear();
                    } else {
                        varTypeList.add(new BoolType());
                        if (ctx.var_val(i).getText().contains("=")) {
                            tt = new TypeTree(ctx.var_val(i).expr(), stt);
                            System.out.println(tt.getRootType());
                            if (BoolType.isBool(tt.getRootType()))
                                varValues.add(true);
                            else {
                                stt.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter boolean to variable ");
                                varValues.add(null);
                            }

                        } else
                            varValues.add(null);
                    }
                return varTypeList;


            case "char":

                for (int i = 0; i < ctx.var_val().size(); i++)
                    if (ctx.var_val(i).getText().contains("[")) {

                        for (int k = 0; k < ctx.var_val(i).Int_const().size(); k++)
                            Parameters.add(Integer.parseInt(ctx.var_val(i).Int_const(k).toString()));

                        int[] x = new int[Parameters.size()];
                        for (int j = 0; j < Parameters.size(); j++) {
                            x[j] = Parameters.get(j);   //Parameters in ArrayList
                        }
                        varTypeList.add(new ArrayType(new CharType(), x));
                        varValues.add(null);
                        Parameters.clear();
                    } else {
                        varTypeList.add(new CharType());
                        if (ctx.var_val(i).getText().contains("=")) {
                            tt = new TypeTree(ctx.var_val(i).expr(), stt);
                            if (CharType.isChar(tt.getRootType()))
                                varValues.add('a');
                            else {
                                stt.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter char to variable ");
                                varValues.add(null);
                            }
                        } else
                            varValues.add(null);
                    }
                return varTypeList;


            case "int":

                for (int i = 0; i < ctx.var_val().size(); i++)
                    if (ctx.var_val(i).getText().contains("[")) {

                        for (int k = 0; k < ctx.var_val(i).Int_const().size(); k++)
                            Parameters.add(Integer.parseInt(ctx.var_val(i).Int_const(k).toString()));
                        int[] x = new int[Parameters.size()];
                        for (int j = 0; j < Parameters.size(); j++) {
                            x[j] = Parameters.get(j);   //Parameters in ArrayList
                        }
                        varTypeList.add(new ArrayType(new IntegerType(), x));
                        varValues.add(null);
                        Parameters.clear();
                    } else {
                        varTypeList.add(new IntegerType());
                        if (ctx.var_val(i).getText().contains("=")) {
                            String v = ctx.var_val(i).expr().getText();
                            tt = new TypeTree(ctx.var_val(i).expr(), stt);
                            if (IntegerType.isInteger(tt.getRootType()))
                                varValues.add(1);
                            else {
                                stt.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter integer to variable ");

                                varValues.add(null);
                            }
                        } else
                            varValues.add(null);

                    }
                return varTypeList;


            case "long":

                for (int i = 0; i < ctx.var_val().size(); i++)
                    if (ctx.var_val(i).getText().contains("[")) {

                        for (int k = 0; k < ctx.var_val(i).Int_const().size(); k++)
                            Parameters.add(Integer.parseInt(ctx.var_val(i).Int_const(k).toString()));

                        int[] x = new int[Parameters.size()];
                        for (int j = 0; j < Parameters.size(); j++) {
                            x[j] = Parameters.get(j);   //Parameters in ArrayList
                        }
                        varTypeList.add(new ArrayType(new LongType(), x));
                        varValues.add(null);
                        Parameters.clear();
                    } else {
                        varTypeList.add(new LongType());
                        if (ctx.var_val(i).getText().contains("=")) {
                            tt = new TypeTree(ctx.var_val(i).expr(), stt);
                            if (LongType.isLong(tt.getRootType())) {
                                varValues.add(1);
                            } else {
                                stt.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter Long to variable ");
                                varValues.add(null);
                            }
                        } else
                            varValues.add(null);

                    }
                return varTypeList;

            case "float":
                for (int i = 0; i < ctx.var_val().size(); i++)
                    if (ctx.var_val(i).getText().contains("[")) {

                        for (int k = 0; k < ctx.var_val(i).Int_const().size(); k++)
                            Parameters.add(Integer.parseInt(ctx.var_val(i).Int_const(k).toString()));

                        int[] x = new int[Parameters.size()];
                        for (int j = 0; j < Parameters.size(); j++) {
                            x[j] = Parameters.get(j);   //Parameters in ArrayList
                        }
                        varTypeList.add(new ArrayType(new FloatType(), x));
                        varValues.add(0);
                        Parameters.clear();
                    } else {
                        varTypeList.add(new FloatType());
                        if (ctx.var_val(i).getText().contains("=")) {
                            String v = ctx.var_val(i).expr().getText();
                            tt = new TypeTree(ctx.var_val(i).expr(), stt);
                            if (tt.getRootType() instanceof FloatType)
                                varValues.add(1.1);
                            else {
                                stt.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter Float to variable ");
                                varValues.add(null);
                            }
                        } else
                            varValues.add(null);
                    }
                return varTypeList;

            case "double":
                for (int i = 0; i < ctx.var_val().size(); i++)
                    if (ctx.var_val(i).getText().contains("[")) {

                        for (int k = 0; k < ctx.var_val(i).Int_const().size(); k++)
                            Parameters.add(Integer.parseInt(ctx.var_val(i).Int_const(k).toString()));

                        int[] x = new int[Parameters.size()];
                        for (int j = 0; j < Parameters.size(); j++) {
                            x[j] = Parameters.get(j);   //Parameters in ArrayList
                        }
                        varTypeList.add(new ArrayType(new DoubleType(), x));
                        varValues.add(0);
                        Parameters.clear();
                    } else {
                        varTypeList.add(new DoubleType());
                        if (ctx.var_val(i).getText().contains("=")) {
                            String v = ctx.var_val(i).expr().getText();
                            tt = new TypeTree(ctx.var_val(i).expr(), stt);
                            if (DoubleType.isDouble(tt.getRootType()))
                                varValues.add(12312999999993.1111111112);
                            else {
                                stt.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter Float to variable ");
                                varValues.add(null);
                            }
                        } else
                            varValues.add(null);
                    }

                return varTypeList;

            case "string":
                int stringSize;
                for (int i = 0; i < ctx.var_val().size(); i++)
                    if (ctx.var_val(i).getText().contains("[")) {

                        for (int k = 0; k < ctx.var_val(i).Int_const().size(); k++)
                            Parameters.add(Integer.parseInt(ctx.var_val(i).Int_const(k).toString()));

                        int[] x = new int[Parameters.size()];
                        for (int j = 0; j < Parameters.size(); j++) {
                            x[j] = Parameters.get(j);   //Parameters in ArrayList
                        }
                        if (ctx.var_val(i).getText().contains("\""))
                            stringSize = ctx.var_val(i).expr().getText().length();
                        else
                            stringSize = 0;
                        varTypeList.add(new ArrayType(new StringType(stringSize), x));
                        varValues.add(0);
                        Parameters.clear();
                    } else {
                        if (ctx.var_val(i).getText().contains("=")) {
                            String v = ctx.var_val(i).expr().getText();
                            tt = new TypeTree(ctx.var_val(i).expr(), stt);
                            if (tt.getRootType() instanceof StringType) {
                                stringSize = v.length();
                                varValues.add("hello");
                            } else {
                                stt.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "You should enter String to variable ");
                                stringSize = 0;
                                varValues.add(null);
                            }
                        } else {
                            stringSize = 0;
                            varValues.add(null);
                        }
                        varTypeList.add(new StringType(stringSize));

                    }

                return varTypeList;

            default:
                System.out.println("type : " + type);
                for (int i = 0; i < ctx.var_val().size(); i++) {
                    varTypeList.add(new UserType(type));
                    if (ctx.var_val(i).expr() != null && ctx.var_val(i).getText().contains("=")) {
                        tt = new TypeTree(ctx.var_val(i).expr(), stt);
                        if (tt.getRootType().getName().equals(type) ) {
                            varValues.add("notEmpty");
                        }
                        else {
                            stt.getCurrentScope().addError(ctx.start.getLine(), ctx.start.getCharPositionInLine(), "Type checking Error");
                            varValues.add(null);
                        }
                    } else
                        varValues.add(null);
                }
                return varTypeList;

        }
    }

}
