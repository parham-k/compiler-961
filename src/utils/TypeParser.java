package utils;

import symbolTable.SymbolTable;
import symbolTable.SymbolTableEntry;
import symbolTable.SymbolTableTree;
import types.*;

import java.util.regex.Pattern;

public class TypeParser {

    public static Type getType(String typeText) {
        Type type;
        switch (typeText) {
            case ("int"):
                type = new IntegerType();
                break;
            case ("bool"):
                type = new BoolType();
                break;
            case ("long"):
                type = new LongType();
                break;
            case ("float"):
                type = new FloatType();
                break;
            case ("double"):
                type = new DoubleType();
                break;
            case ("char"):
                type = new CharType();
                break;
            case ("string"):
                type = new StringType(0);
                break;
            default:
                type = new UserType(typeText);
        }
        return type;
    }

    public static Type getValueType(String s, SymbolTableTree tree) {
        if (s.contains("\""))
            return new StringType(s.length() - 2);
        else if (s.contains("'"))
            return new CharType();
        else if (s.equals("true") || s.equals("false"))
            return new BoolType();
        else if (s.contains("."))
            return new FloatType();
        else if (s.matches("[0-9]*") || s.matches("0[xX][0-9A-Fa-f]+"))
            return new IntegerType();
        else {
            SymbolTableEntry entry = tree.find(s);
            if (entry != null)
                return entry.getType();
            else {
                tree.getCurrentScope().addError(0, 0, "Identifier " + s + " not defined");
                return null;
            }
        }
    }

}
