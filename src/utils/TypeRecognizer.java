package utils;

import enums.types;

public class TypeRecognizer {

    /**
     * Created by Alireza on 1/26/2018.
     */



    public static enums.types TypeRecognize (String input)
    {
        if(input.equals("false") || input.equals("true"))
            return types.t_bool;

        if( (input.charAt(0) == '\'')   &&  (input.charAt(input.length()-1) == '\'') && input.length() <= 3 )
            return types.t_char;

        if( (input.charAt(0) == '\"')   &&  (input.charAt(input.length()-1) == '\"'))
            return types.t_string;

        try {
        Integer.parseInt(input);
            return types.t_int;
        }catch (NumberFormatException e)
        {

        }

        try {
            Long.parseLong(input);
            return types.t_long;
        }catch(NumberFormatException e)
        {

        }

        try {
            Float.parseFloat(input);
            return types.t_float;
        }catch (NumberFormatException e)
        {

        }

        try {
            Double.parseDouble(input);
            return types.t_double;
        }catch (NumberFormatException e)
        {

        }

        return types.t_0;
    }


}
