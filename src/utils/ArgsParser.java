package utils;

import lulu2.LULU2Parser;
import symbolTable.SymbolTableEntry;
import types.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ArgsParser {

    public static Map parseArgsVar(LULU2Parser parser, ArrayList<LULU2Parser.Args_varContext> contexts) {
        Map<String, SymbolTableEntry> symbols = new HashMap<>();
        for (LULU2Parser.Args_varContext avc : contexts) {
            String typeText = parser.getTokenStream().getText(avc.type());
            String idName = avc.Identifier().getText();
            Type type = TypeParser.getType(typeText);
            symbols.put(idName, new SymbolTableEntry(type, null));
        }
        return symbols;
    }

    public static ArrayList<SymbolTableEntry> parseArgs(LULU2Parser parser, ArrayList<LULU2Parser.ArgsContext> contexts) {
        ArrayList<SymbolTableEntry> symbols = new ArrayList<>();
        for (LULU2Parser.ArgsContext avc : contexts) {
            String typeText = parser.getTokenStream().getText(avc.type());
            Type type = TypeParser.getType(typeText);
            symbols.add(new SymbolTableEntry(type, null));
        }
        return symbols;
    }

}
