package gui;

import lulu2.LULU2BaseListener;
import lulu2.LULU2Lexer;
import lulu2.LULU2Parser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;

public class CodeInput extends JFrame {

    public CodeInput() {
        super("Input LULU2 Code");
        JTextArea txtCode = new JTextArea();
        txtCode.setTabSize(4);
        txtCode.setFont(new Font("Consolas", Font.PLAIN, 16));
        JButton btnStart = new JButton("START");
        setLayout(new BorderLayout());
        add(new JScrollPane(txtCode), BorderLayout.CENTER);
        add(btnStart, BorderLayout.SOUTH);
        btnStart.addActionListener((ActionEvent ae) -> {
            try {
                ANTLRInputStream input = new ANTLRInputStream(txtCode.getText());
                LULU2Lexer lexer = new LULU2Lexer(input);
                CommonTokenStream tokens = new CommonTokenStream(lexer);
                LULU2Parser parser = new LULU2Parser(tokens);
                ParseTree tree = parser.program();
                ParseTreeWalker walker = new ParseTreeWalker();
                LULU2BaseListener listener = new LULU2BaseListener(parser);
                walker.walk(listener, tree);
                SymbolTableTreeGUI gui = new SymbolTableTreeGUI(listener.getSymbolTableTree());
                gui.showWindow(txtCode.getText());
                setVisible(false);
            } catch (Exception exc) {
                exc.printStackTrace();
                JOptionPane.showMessageDialog(this, exc.getMessage(), "Exception", JOptionPane.ERROR_MESSAGE);
            }
        });
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem itemOpen = new JMenuItem("Open");
        itemOpen.addActionListener((ActionEvent e) -> {
            JFileChooser open = new JFileChooser(CodeInput.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            if (open.showOpenDialog(this) != JFileChooser.CANCEL_OPTION) {
                File inputFile = open.getSelectedFile();
                try {
                    BufferedReader reader = new BufferedReader(new FileReader(inputFile));
                    String line;
                    txtCode.setText("");
                    while ((line = reader.readLine()) != null)
                        txtCode.append(line + "\n");
                    reader.close();
                } catch (FileNotFoundException ex1) {
                    JOptionPane.showMessageDialog(this, "File not found", "Error", JOptionPane.ERROR_MESSAGE);
                } catch (IOException ex2) {
                    JOptionPane.showMessageDialog(this, ex2.getMessage(), "IOException", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        fileMenu.add(itemOpen);
        JMenuItem itemExit = new JMenuItem("Exit");
        itemExit.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });
        fileMenu.add(itemExit);
        menuBar.add(fileMenu);
        setJMenuBar(menuBar);
        setSize(400, 400);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

}
