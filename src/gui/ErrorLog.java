package gui;

import symbolTable.Scope;
import symbolTable.SymbolTable;
import symbolTable.SymbolTableTree;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.Queue;

public class ErrorLog extends JFrame {

    ErrorLog(SymbolTableTree tree) {
        super("Error log");
        Queue<String> errors = BFS(tree);
        JTextArea txtErrors = new JTextArea();
        txtErrors.setFont(new Font("Consolas", Font.PLAIN, 16));
        for (String err : errors)
            txtErrors.append(err + "\n");
        txtErrors.setEditable(false);
        setLayout(new GridLayout());
        add(txtErrors);
        setSize(400, 400);
        setVisible(true);
    }

    private Queue<String> BFS(SymbolTableTree tree) {
        Queue<Scope> q = new LinkedList<>();
        Queue<String> errors = new LinkedList<>();
        q.add(tree.getGlobal());
        while (q.size() > 0) {
            Scope s = q.remove();
            errors.addAll(s.getErrors());
            q.addAll(s.getChildren());
        }
        return errors;
    }

}
