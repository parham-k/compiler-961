package gui;

import symbolTable.Scope;
import symbolTable.SymbolTableTree;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.text.DecimalFormat;

class Node extends DefaultMutableTreeNode {

    private Scope scope;

    Node(Scope scope) {
        super((scope.hasError() ? "! " : "") + scope.getName());
        this.scope = scope;
    }

    Scope getScope() {
        return scope;
    }

    JTable getGUITable() {
        return scope.symbolTable().tableGUI();
    }

    JTextArea getGUIErrors() {
        JTextArea txtErrors = new JTextArea();
        for (String err : scope.getErrors())
            txtErrors.append(err + "\n");
        return txtErrors;
    }

}

public class SymbolTableTreeGUI extends JFrame {

    private SymbolTableTree tree;

    SymbolTableTreeGUI(SymbolTableTree symbolTableTree) {
        super("LULU2 Compiler");
        this.tree = symbolTableTree;
        Node root = new Node(symbolTableTree.getCurrentScope());
        fillTree(root);
        JTree tree = new JTree(root);
        tree.setCellRenderer(new TreeRenderer());
        JPanel pnlMiddle = new JPanel(new BorderLayout());
        JLabel lblStatus = new JLabel("GLOBAL, width = " + symbolTableTree.getGlobal().getWidth());
        pnlMiddle.add(lblStatus, BorderLayout.NORTH);
        JPanel pnlTable = new JPanel(new GridLayout(2, 0));
        pnlTable.add(new JScrollPane(symbolTableTree.getGlobal().symbolTable().tableGUI()));
        tree.setPreferredSize(new Dimension(200, 400));
        tree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                TreePath tp = tree.getPathForLocation(e.getX(), e.getY());
                if (tp != null) {
                    pnlTable.removeAll();
                    Node clicked = (Node) tp.getLastPathComponent();
                    lblStatus.setText(tp.toString() + ", width = " + clicked.getScope().getWidth());
                    pnlTable.add(new JScrollPane(clicked.getGUITable()));
                    pnlTable.add(new JScrollPane(clicked.getGUIErrors()));
                    pnlTable.revalidate();
                }
            }
        });
        setLayout(new BorderLayout());
        add(new JScrollPane(tree), BorderLayout.WEST);
        pnlMiddle.add(pnlTable, BorderLayout.CENTER);
        add(pnlMiddle, BorderLayout.CENTER);
        setJMenuBar(menuBar());
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void fillTree(Node n) {
        if (n != null)
            for (Scope s : n.getScope().getChildren()) {
                Node child = new Node(s);
                n.add(child);
                fillTree(child);
            }
    }

    public void showWindow(String code) {
        this.setSize(600, 600);
        this.setVisible(true);

        JTextArea txtCode = new JTextArea();
        String[] lines = code.split("\n");
        DecimalFormat format = new DecimalFormat("00");
        for (int i = 0; i < lines.length; i++)
            txtCode.append(format.format(i + 1) + " : " + lines[i] + "\n");
        txtCode.setEditable(false);
        txtCode.setPreferredSize(new Dimension(300, 400));
        txtCode.setFont(new Font("Consolas", Font.PLAIN, 14));
        add(new JScrollPane(txtCode), BorderLayout.EAST);
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
        this.invalidate();
    }

    private JMenuBar menuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        menuBar.add(fileMenu);
        JMenuItem itemNew = new JMenuItem("New", KeyEvent.VK_N);
        itemNew.addActionListener((ActionEvent e) -> {
            new CodeInput();
            setVisible(false);
        });
        fileMenu.add(itemNew);
        JMenuItem itemExit = new JMenuItem("Exit", KeyEvent.VK_X);
        itemExit.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });
        fileMenu.add(itemExit);
        JMenu codeMenu = new JMenu("Code");
        codeMenu.setMnemonic(KeyEvent.VK_C);
        JMenuItem itemErrors = new JMenuItem("Error log");
        itemErrors.setMnemonic(KeyEvent.VK_E);
        itemErrors.addActionListener((ActionEvent e) -> {
            new ErrorLog(tree);
        });
        codeMenu.add(itemErrors);
        menuBar.add(codeMenu);
        return menuBar;
    }

}

class TreeRenderer implements TreeCellRenderer {
    private JLabel label;

    TreeRenderer() {
        label = new JLabel();
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
                                                  boolean leaf, int row, boolean hasFocus) {
        Object o = ((DefaultMutableTreeNode) value).getUserObject();
        URL imageUrl;
        if(selected)
            imageUrl = getClass().getResource("selected.png");
        else if (o.toString().charAt(0) == '!')
            imageUrl = getClass().getResource("error.png");
        else
            imageUrl = getClass().getResource("code.png");
        label.setIcon(new ImageIcon(imageUrl));
        label.setText("" + value);
        return label;
    }
}