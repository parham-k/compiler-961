package gui;

import symbolTable.SymbolTable;

import javax.swing.*;
import java.awt.*;

public class SymbolTableGUI extends JFrame {

    public SymbolTableGUI(SymbolTable symbolTable) {
        JTable table = symbolTable.tableGUI();
        setLayout(new GridLayout());
        add(new JScrollPane(table));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public void showWindow() {
        setSize(400, 400);
        setVisible(true);
    }

}
