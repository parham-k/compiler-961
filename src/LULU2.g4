grammar LULU2																						                    ;
Comments_sl		: '%%' .*? '\n' 				-> skip												                    ;
Comments_ml		: '%@'.*? '@%'					-> skip												                    ;
WhiteSpace		: (' '|'\n'|'\r'|'\t')+			-> skip												                    ;
Identifier		: [A-Za-z_][A-Za-z_0-9]*										                	                    ;

program			: ft_dcl?ft_def*																	                    ;
ft_dcl			: 'declare''{'(func_dcl|type_dcl|var_def)+'}' 										                    ;
func_dcl		: ('('args')''=')?Identifier'('(args|args_var)?')'';'								                    ;

args			: type('['']')* | args','type('['']')*										                            ;

args_var		: (type)(Identifier)('['']')*|args_var','(type)(Identifier)('['']')* 				                    ;

type			: 'int' | 'bool' | 'float' | 'long' | 'char' | 'double' | 'string' | Identifier		                    ;
type_dcl		: Identifier';'																		                    ;
var_def			: 'const'?type var_val(',' var_val)*';'												                    ;
var_val			: Identifier('[' Int_const ']')*('='(expr|list|'allocate'Identifier))?				                    ;
list			: '['(expr|list)(','(expr|list))*']'												                    ;
ft_def			: (type_def|fun_def)+																                    ;
type_def		: 'type'(Identifier)(':'Identifier)?'{'(component)+'}'								                    ;
component		: (access_modifier)?(var_def|fun_def)												                    ;
access_modifier	: 'private'|'public'|'protected'													                    ;
fun_def			: ('('args_var')''=')?'function'Identifier'('(args_var)?')'block					                    ;
block			: '{'(var_def|stmt)*'}'																                    ;
stmt			: assign';' | func_call';' | cond_stmt | loop_stmt | 'return'';' |
					jump ';' | label | expr';' | 'break'';' | 'continue'';' |
					'destruct'('['']')*Identifier';'												                    ;

assign 			: var '=' assign_f | '(' var ( ',' var )* ')' '=' func_call							                    ;
assign_f		: expr | 'new'																		                    ;

var 			: ( ('this' | 'super') '.') ? ref ( '.' ref )* 										                    ;
ref 			: Identifier ( '[' expr ']' )* 														                    ;

expr            : expr_lvl1 | expr '||' expr_lvl1;
expr_lvl1       : expr_lvl2 | expr_lvl1 '&&' expr_lvl2;
expr_lvl2       : expr_lvl3 | expr_lvl2 '|' expr_lvl3;
expr_lvl3       : expr_lvl4 | expr_lvl3 '^' expr_lvl4;
expr_lvl4       : expr_lvl5 | expr_lvl4 '&' expr_lvl5;
expr_lvl5       : expr_lvl6 | expr_lvl5 relational1 expr_lvl6;
expr_lvl6       : expr_lvl7 | expr_lvl6 relational2 expr_lvl7;
expr_lvl7       : expr_lvl8 | expr_lvl7 '+' expr_lvl8 | expr_lvl7 '-' expr_lvl8;
expr_lvl8       : expr_lvl9 | expr_lvl8 muldivmud expr_lvl9;
expr_lvl9       : unary_op expr_lvl9 | expr_lvl10;
expr_lvl10      : '('expr')' | const_val | var;

func_call 		: ( var '.' ) ? Identifier '(' params ? ')' | 'sizeof' '(' ( type | var ) ')' |	
					'read' '(' var ')' | 'write' '(' var ')' 										                    ;

params 			: expr params_f																		                    ;
params_f		: ',' params | 																		                    ;

cond_stmt       : 'if' '(' expr ')' block ( 'else' block ) ? |
                    'switch' '(' var ')' 'of' ':' '{' ( 'case' Int_const ':' block )* 'default' ':' block '}' ;
loop_stmt 		: 'for' '(' var_def ? ';' expr ';' assign ? ')' block | 'while' '(' expr ')' block 	                    ;

jump 			: 'goto' Identifier 																                    ;

label 			: Identifier ':'																	                    ;
const_val 		: Int_const | Real_const | Char_const | bool_const | String_const					                    ;
unary_op 		: '-' | '!' | '~'																	                    ;
relational1 	: '==' | '!='                                                                                           ;
relational2     : '>=' | '<=' | '<' | '>'											            	                    ;
muldivmud       : '*' | '/' | '%' ;
Int_const		: ([1-9])([0-9])* | '0'	| '0'('x'|'X')[0-9A-Fa-f]*									                    ;
Real_const		: [0-9]*'.'[0-9]*(('E'|'e')('+'|'-')?[0-9]+)?										                    ;
Char			: [\\]. | ('\\X'|'\\x')[A-Fa-f0-9]+ | ~('\''|'\\'|'"')								                    ;
Char_const		: '\''Char?'\''																		                    ;
bool_const		: 'true' | 'false'																	                    ;
String_const	: '"'Char*'"'																		                    ;

keyword			:     'allocate'    | 'bool'    | 'break'   | 'case'    | 'char'        | 'const'   | 'continue'
					| 'declare'     | 'default' | 'destruct'| 'double'  | 'else'        | 'false'
					| 'function'    | 'float'   | 'for'     | 'goto'    | 'if'          | 'int'     | 'long'
					| 'new'         | 'nil'     | 'of'      | 'private' | 'protected'   | 'public'
					| 'read'        | 'return'  | 'sizeof'  | 'string'  | 'super'       | 'switch'

					| 'this'        | 'true'    | 'while'   | 'write'   | 'type'		   							    ;